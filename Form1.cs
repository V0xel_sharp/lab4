﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
   
    public partial class Form1 : Form
    {
        public BindingList<Basic> basics;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitBindingList();
            dataGridView1.DataSource = basics;

        }
        private void InitBindingList()
        {
            basics = new BindingList<Basic>();

            basics.AllowNew = true;
            basics.AllowRemove = false;
            basics.RaiseListChangedEvents = true;
            basics.AllowEdit = true;

            basics.Add(new Basic("Jakub", 5, 3.4));
            basics.Add(new Basic("Agata", 4, 5.0));
            basics.Add(new Basic("Szymon", 7, 4.4));
            basics.Add(new Basic("Anna", 2, 3.8));
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textString.Text) || 
                string.IsNullOrEmpty(textInt.Text) || 
                string.IsNullOrEmpty(textFloat.Text))
            {
                MessageBox.Show("Puste pola!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var txt = textString.Text;
            int val = int.Parse(textInt.Text);
            double prec = double.Parse(textFloat.Text);

            basics.Add(new Basic(txt, val, prec));
        }

        private void buttonSort_Click(object sender, EventArgs e)
        {
            //List<Basic> sortedBasics = basics.OrderBy(x => x.Value).ToList(); //Other option, sort by LINQ
            List<Basic> sortedBasics = basics.ToList();
            sortedBasics.Sort();
            basics = new BindingList<Basic>(sortedBasics);
            dataGridView1.DataSource = basics;
        }
    }
}
