﻿using System;

namespace Lab4
{
    public class Basic : IComparable<Basic>
    {
        public Basic(string txtName, int val, double prec)
        {
            Name = txtName;
            Value = val;
            Precision = prec;
        }

        public Basic()
        {
            Name = "Not Set";
            Value = -1;
            Precision = 0.0;
        }
       
        public string Name { get; set; }
        public int Value { get; set; }
        public double Precision { get; set; }

        /*public int Compare(Basic a, Basic b)
        {
            //Just change Value for whatever else
            if (a.Value.CompareTo(b.Value) != 0)
            {
                return a.Value.CompareTo(b.Value);
            }
            else if (a.Value.CompareTo(b.Value) != 0)
            {
                return a.Value.CompareTo(b.Value);
            }
            else
                return 0;
        }*/

        public int CompareTo(Basic other)
        {
            if (this.Value > other.Value) return -1;
            if (this.Value == other.Value) return 0;
            return 1;
        }
    }
}
